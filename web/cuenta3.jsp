<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/tlds/miBiblioteca" prefix="fun" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP cuenta biblioteca personalizada</title>
    </head>
    <body>
        <c:set var="atrCuenta" scope="application" value="${fun:incUno(atrCuenta)}" />
        
        <h1>Demostración de seguimiento a nivel de aplicación</h1>
        
        ${param.parNombre}, has visitado esta página
        ${atrCuenta} ${(atrCuenta > 1) ? " veces." : " vez."}
    </body>
</html>
