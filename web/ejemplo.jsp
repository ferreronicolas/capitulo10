
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP 2.0</title>
    </head>
    <body>
        <h3>Lenguaje de expresión (LE)</h3>
        <p>La siguiente tabla ilustra algunas expresiones LE:
        <br><br><table border="1">
            <tr>
                <th><b>Expresión</b></th>
                <th><b>Valor</b></th>
            </tr>
            <tr>
                <td>\${3+6}</td>
                <td>${3+6}</td>
            </tr>
            <tr>
                <td>\${(7 > 4) ? 7 : 4}</td>
                <td>${(7 > 4) ? 7 : 4}</td>
            </tr>
            <tr>
                <td>\${header["host"]}</td>
                <td>${header["host"]}</td>
            </tr>
        </table>
    </body>
</html>
