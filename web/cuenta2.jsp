<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP cuenta con JSTL</title>
    </head>
    <body>
        <c:set var="atrCuenta" scope="application" value="${atrCuenta + 1}" />
        
        <h1>Demostración de seguimiento a nivel aplicación
            (con JSTL)</h1>
        
        ${param.parNombre}, Has visitado esta página 
        ${atrCuenta} ${(atrCuenta > 1) ? " veces." : " vez."}
    </body>
</html>
