
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BD Tutorias</title>
    </head>
    <body>
        <%-- Conectar con la base de datos. Resultado: bd --%>>
        <sql:setDataSource var="bd"
                           driver="com.mysql.jdbc-Driver"
                           url="jdbc:mysql://localhost:3306/bd_tutorias"
                           user="root"
                           password="123456" />
        
        <%-- Realizar una consulta. Resultado: cdr --%>
        <sql:query var="cdr" dataSource="${bd}" >
            SELECT profesores, dia, hora, alumno
                FROM profesores, tutorias, citas
                WHERE profesores.id_profesor = tutorias.id_profesor
                    AND tutorias.id_tutoria = citas.id_tutoria
        </sql:query>

        <%-- Mostrar el resultado cdr en una tabla --%>
        <table width=100% border="1">
            <%-- Cabeceras --%>
            <tr>
                <th>Alumno</th><th>Dia/hora</th><th>Con el profesor</th>
            </tr>
            <%-- Filas --%>
            <c:forEach var="fila" items="${cdr.rows}">
                <tr>
                    <td width=40%>${fila.alumno}</td>
                    <td width=20%>${fila.dia} a las ${fila.hora}</td>
                    <td width=40%>${fila.profesor}</td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
