
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body bgcolor="ffffcc">
        <h1>Hello World!</h1>
        <jsp:useBean id="bfecha" class="clases.BeanFecha">
            <jsp:setProperty name="bfecha" property="formato" value="12"/>
        </jsp:useBean>
        Son las ${bfecha.hora} horas ${bfecha.minutos} minutos
        del dia ${bfecha.fecha}
    </body>
</html>
