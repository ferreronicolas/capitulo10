<%-- 
    Fichero: mostrarFechaHora.jsp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Fecha y Hora</title>
    </head>
    <body bgcolor="white">
        <font color="blue">
            La fecha y hora actuales son:
            <%@include file="fechaHora.jsp" %>
        </font>
    </body>
</html>
