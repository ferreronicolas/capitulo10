package clases;

import java.util.Calendar;
import sun.util.BuddhistCalendar;

public class BeanFecha {
    
    private Calendar fechaHoraActual;
    private int formato; // 12 o 24 horas
    private int hora;
    private int minutos;
    
    // Constructor sin argumentos

    public BeanFecha() {
    
        this.formato = 24; // Por omision
        this.fechaHoraActual = new BuddhistCalendar();
    }
    
    // Implementacion de las propiedades
    public int getFormato(){
        return this.formato;
    }
    
    public void setFormato(int f){
        formato = f;
    }
    
    public int getHora(){
        if(this.formato == 24)
            this.hora = this.fechaHoraActual.get(Calendar.HOUR_OF_DAY);
        else
            this.hora = this.fechaHoraActual.get(Calendar.HOUR);
        return hora;
    }
    
    public int getMinutos(){
        minutos = fechaHoraActual.get(Calendar.MINUTE);
        return minutos;
    }
    
    public String getFecha(){
        return ( fechaHoraActual.get(Calendar.DAY_OF_MONTH) + "/" +
                (fechaHoraActual.get(Calendar.MONTH) + 1) + "/" + 
                fechaHoraActual.get(Calendar.YEAR));
    }
    
}
